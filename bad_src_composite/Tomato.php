<?php

require_once 'Kebab.php';

class Tomato implements Kebab
{
	private $kebab;

	public function __construct(Kebab $kebab)
	{
		$this->kebab = $kebab;
	}

	public function removeOnion()
	{
		return new self($this->kebab->removeOnion());
	}

	public function doubleCheese()
	{
		return new self($this->kebab->doubleCheese());
	}

	public function yoloprint()
	{
		return 'Tomato | ' . $this->kebab->yoloprint();
	}
}