<?php

require_once 'Kebab.php';

class Barquette implements Kebab // NullObject
{
	public function removeOnion()
	{
		return $this;
	}

	public function doubleCheese()
	{
		return $this;
	}

	public function yoloprint()
	{
		return 'Barquette';
	}
}