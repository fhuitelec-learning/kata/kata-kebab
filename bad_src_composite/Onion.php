<?php

require_once 'Kebab.php';

class Onion implements Kebab
{
	/** Kebab */
	private $kebab;

	public function __construct(Kebab $kebab)
	{
		$this->kebab = $kebab; // tomato->barquette
	}

	public function removeOnion()
	{
		return $this->kebab->removeOnion();
	}

	public function doubleCheese()
	{
		return new self($this->kebab->doubleCheese());
	}

	public function yoloprint()
	{
		return 'Onion | ' . $this->kebab->yoloprint();
	}
}