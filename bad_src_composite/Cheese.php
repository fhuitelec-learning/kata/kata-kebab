<?php

require_once 'Kebab.php';

class Cheese implements Kebab
{
	private $kebab;

	public function __construct(Kebab $kebab)
	{
		$this->kebab = $kebab;
	}

	public function removeOnion()
	{
		return new self($this->kebab->removeOnion());
	}

	public function doubleCheese()
	{
		return new self(
			new self(
				$this->kebab->doubleCheese()
			)
		);
	}

	public function yoloprint()
	{
		return 'Cheese | ' . $this->kebab->yoloprint();
	}
}