<?php

interface Kebab
{
	public function removeOnion();
	public function doubleCheese();
	public function yoloprint();
}