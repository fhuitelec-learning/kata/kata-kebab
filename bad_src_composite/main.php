<?php

require_once 'Kebab.php';
require_once 'Barquette.php';
require_once 'Onion.php';
require_once 'Cheese.php';
require_once 'Tomato.php';

function removeOnionAndCheeseDoubleExample()
{
	$kebab =
		new Onion(
			new Cheese(
				new Tomato(
					new Cheese(
						new Barquette()
					)
				)
			)
		);

	echo $kebab
		->removeOnion()
		->doubleCheese()
		->yoloprint();
}

removeOnionAndCheeseDoubleExample();