# Kebab kata - possible solution

## Todo

- [ ] Refacto Pescatarian to remove inheritance
- [ ] Implement `Kebab::doubleCheese()`
- [ ] Implement `Kebab::removeOnions()`
- [ ] Functional test the whole kebab features
- [ ] Unit test every classes

## Design thinking

### Avantages du visiteur vs. composite

#### + d'ingrédient
- visiteur 
- composite

#### + de comportement (régime/modification générique du kebab)

### Pourquoi marcher par exclusion dans les régimes ?

Moins d'ingrédient à exclure qu'à accepter.
Naturellement, les régimes interdisent des aliments plutôt que ne les acceptent.

Même si c'est rare, pour les futurs ajouts d'ingrédient, seuls les régimes concernés par l'ingrédient sont à modifier.