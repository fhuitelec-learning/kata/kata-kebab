<?php
namespace Kata\Kebab;

use Assert\Assert;
use ReflectionClass;

final class Ingredient
{
    // Pure ingredient
    const ONION = 'onion';
    const CHEESE = 'cheese';
    const TOMATO = 'tomato';
    const LETTUCE = 'lettuce';
    const FISH = 'fish';
    const CHICKEN = 'chicken';
    const BEEF = 'beef';
    const LAMB = 'lamb';
    // Sauces
    const SAMOURAI_SAUCE = 'samourai';
    const MAYONNAISE_SAUCE = 'mayonnaise';

    /** @var string */
    private $name;

    /**
     * @param string $name
     */
    private function __construct($name)
    {
        Assert::that($name)
            ->inArray($this->acceptedIngredientNames());

        $this->name = $name;
    }

    /**
     * @param string $name
     * @return Ingredient
     */
    public static function fromIngredientName($name) {
        return new self($name);
    }

    /**
     * @param Diet $diet
     * @return bool
     */
    public function isForbiddenBy(Diet $diet)
    {
        return $diet->doesForbid($this);
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    private function acceptedIngredientNames()
    {
        return (new ReflectionClass(self::class))->getConstants();
    }
}