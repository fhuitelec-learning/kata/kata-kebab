<?php
namespace Kata\Kebab\Diet;

use Kata\Kebab\Diet;
use Kata\Kebab\Ingredient;

class Pescatarian extends Diet
{
    /** @var string[] */
    protected $forbiddenIngredients = [
        Ingredient::CHICKEN,
        Ingredient::BEEF,
        Ingredient::LAMB,
    ];
}