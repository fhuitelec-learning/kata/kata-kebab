<?php
namespace Kata\Kebab\Diet;

use Kata\Kebab\Diet;
use Kata\Kebab\Ingredient;

class Vegetarian extends Diet
{
    /** @var string[] */
    protected $forbiddenIngredients = [
        Ingredient::FISH,
        Ingredient::CHICKEN,
        Ingredient::BEEF,
        Ingredient::LAMB,
    ];
}