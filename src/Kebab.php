<?php

namespace Kata\Kebab;

use Kata\Kebab\Diet\Pescatarian;
use Kata\Kebab\Diet\Vegetarian;

class Kebab
{
    /** @var Ingredient[] */
    private $ingredients = [];

    /**
     * @param array $ingredients
     */
    public function __construct(array $ingredients)
    {
        $this->ingredients = $ingredients;
    }

    /**
     * @return bool
     */
    public function isVegetarian()
    {
        return $this->checkDiet(new Vegetarian());
    }

    /**
     * @return bool
     */
	public function isPescatarian()
    {
	    return $this->checkDiet(new Pescatarian());
    }

    /**
     * @param Diet $diet
     * @return bool
     */
	private function checkDiet(Diet $diet)
    {
        foreach($this->ingredients as $ingredient) {
            if ($ingredient->isForbiddenBy($diet)) {
                return false;
            }
        }

        return true;
    }
}