<?php

namespace Kata\Kebab;

abstract class Diet
{
    protected $forbiddenIngredients = [];

    /**
     * @param Ingredient $ingredient
     * @return bool
     */
    public function doesForbid(Ingredient $ingredient)
    {
        return in_array(
            $ingredient->name(),
            $this->forbiddenIngredients
        );
    }
}