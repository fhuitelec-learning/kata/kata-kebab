<?php
namespace Kata\Kebab\Tests\Functional\Helper;

use Kata\Kebab\Ingredient;
use Kata\Kebab\Kebab;

class KebabBuilder
{
    /** @var Ingredient[] */
    private $ingredients;

    /**
     * KebabBuilder constructor.
     */
    public function __construct()
    {
        $this->ingredients = [];
    }

    /**
     * @param $ingredient
     * @return $this
     */
    public function withIngredient($ingredient)
    {
        $this->ingredients[] = Ingredient::fromIngredientName($ingredient);
        return $this;
    }

    public function build()
    {
        return new Kebab($this->ingredients);
    }
}