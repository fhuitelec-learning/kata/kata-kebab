<?php
namespace Kata\Kebab\Tests\Functional;

use Kata\Kebab\Ingredient;
use Kata\Kebab\Tests\Functional\Helper\KebabBuilder;

class KebabTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function it_is_vegetarian()
    {
        $kebab = (new KebabBuilder())
            ->withIngredient(Ingredient::ONION)
            ->withIngredient(Ingredient::TOMATO)
            ->build();

        $this->assertTrue($kebab->isVegetarian());
    }

    /**
     * @test
     */
    public function it_is_pescatarian()
    {
        $kebab = (new KebabBuilder())
            ->withIngredient(Ingredient::FISH)
            ->withIngredient(Ingredient::ONION)
            ->withIngredient(Ingredient::TOMATO)
            ->build();

        $this->assertTrue($kebab->isPescatarian());
    }
}